#include "listaJugadores.h"

#include <iostream>
#include <string>
using namespace std;

bool listaJugadoresLlena(const tListaJugadores &listaJug, bool &llena)
{
    if(listaJug.contador == LISTA_JUGADORES_MAX) llena = true;
    else llena = false;
    return llena;
}

void buscarJugador(const tListaJugadores &listaJug, string nombre, bool &encontrado, int &posicion)
{
    int inicio= 0, final = listaJug.contador-1, medio;
    encontrado = false;

    while((inicio <= final) && !encontrado)
    {
        medio = (inicio + final)/2;
        if(nombre == listaJug.jugadores[medio].nombre) encontrado = true;
        else if(nombre < listaJug.jugadores[medio].nombre) final = medio - 1;
        else inicio = medio + 1;
    }
    if(encontrado) posicion = medio;
    else posicion = final + 1;
}

bool insertaJugador(tJugador nuevoJugador, tListaJugadores &listaJug, bool &insertaJug)
{
    bool encontrado;
    int posicion;

    if(listaJug.contador == LISTA_JUGADORES_MAX)
    {
        insertaJug = false;
    }
    else
    {
        buscarJugador(listaJug, nuevoJugador.nombre, encontrado, posicion);
        if(encontrado == true)
        {
            insertaJug = false;
        }
        else
        {
            for(int i = listaJug.contador; i > posicion; i--)
            {
                listaJug.jugadores[i] = listaJug.jugadores[i-1];
            }
            listaJug.jugadores[posicion] = nuevoJugador;
            listaJug.contador++;
            insertaJug = true;
        }
    }
    return insertaJug;
}

/*bool bajaJugador(cstring nombre, tListaJugadores &listaJug , tListaEdificios &edificios){
}*/

//vale