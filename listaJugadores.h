#ifndef LISTAJUGADORES_H
#define LISTAJUGADORES_H

#include "listaEdificios.h"

const unsigned int LISTA_JUGADORES_MAX = 20; //numero maximo de jugadores

typedef struct{
    string nombre;
    string contrasenya;
    string universidad;
    int dinero;
    int prestigio;
	int comprados[LISTA_EDIFICIOS_MAX];
    int edificios[50];
} tJugador;

typedef struct{
	tJugador jugadores[LISTA_JUGADORES_MAX];
    int contador;
} tListaJugadores;


/*Lee los datos de un jugador, inicializa su lista de edificios comprados como vacia, el dinero a 3000 creditos y los puntos de prestigio a 0
Devuelve la estructura tJugador debidamente inicializada*/
void nuevoJugador(tJugador &jugador);

/*Dado un tJugador y la lista de edificios, muestra en la consola los datos del jugador y los datos basicos del jugador y los datos basicos de todos sus edificios
(nombre, dinero y prestigio por turno*/
void mostrarJugador(const tJugador jugador, tListaEdificios &edificios, bool mostrarInfo);

//Recibe como entrada un tJugador y devuelve un booleano indicando si su lista de edificios comprados esta llena
bool listaCompradosLlena(const tJugador &jug, bool &llena);

/*Recibe como entrada un tJugador, un codigo de edificio y la lista completa de edificios. Si se puede hacer la compra (el edicio existe,
esta disponible y hay dinero suficiente) realiza la compra y a�ade el codigo de edificio a la lista de edificios del jugador
*/
void comprarEdificio(const tJugador &jug, int codigo, tListaEdificios &edificios);

//funcion que recibe como parametro una lista de jugadores y devuelve un booleano indicando si la lista esta llena o no
bool listaJugadoresLlena(const tListaJugadores &listaJug, bool &llena);

/*Funcion que rebibe como parametro de entrada una lista de jugadores y un nombre de usuario. Devuelve un booleano indicando si se ha encontrado
el jugador o no y, en caso afirmativo, la posicion en la que se encuentra el jugador*/
void buscarJugador(const tListaJugadores &listaJug, string nombre, bool &encontrado, int &posicion);

//Recibe una variable de un tipo tJugador y una lista de jugadores. Inserta el jugador al final de la lista de jugadores
bool insertaJugador(tJugador nuevoJugador, tListaJugadores &listaJug, bool &insertaJug);

//Recibe un nombre de usuario, la lista de jugadores y la lista de edificios. Busca al jugador en la lista y lo elimina completamente
bool bajaJugador(string nombre, tListaJugadores &listaJug, tListaEdificios &edificios);

#endif
