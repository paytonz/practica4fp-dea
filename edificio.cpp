#include <iostream>
#include <fstream>
#include <string>
#include "edificio.h"
#include "listaEdificios.h"
using namespace std;



tEdificio nuevoEdificio(void)
	/*No recibe ning�n valor de entrada. Pide al usuario los datos de un edificio y devuelve como salida una estructura tEdificio debidamente inicializada.*/
{
	tEdificio nuevo_edificio;
	do
	{
		cout << "Introduce codigo:";
		cin >> nuevo_edificio.codigo;
	}
	while (nuevo_edificio.codigo<=0);
	{

		cout << "Introduce nombre:";
		cin  >>  nuevo_edificio.nombre;
		do
		{
			cout << "Introduce precio:";
			cin  >> nuevo_edificio.precio;
		}
		while (nuevo_edificio.precio<=0);


		cout << "Introduce dinero que genera el edificio :";
		cin  >> nuevo_edificio.dinero;

		cout << "Introduce prestigio que genera el edificio :";
		cin  >> nuevo_edificio.prestigio;
		int aux;
		do
		{

			cout << "Introduce el estado del edificio:";
			cout << "0.disponible" << endl <<   "1.comprado" << endl<< "2.borrado" <<endl;
			cin  >>  aux;
		}
		while(aux<0 || aux > 3);

		nuevo_edificio.estadoEdificio=(tEstadoEdificio)aux;

		return nuevo_edificio;
	}
}
/*void cargaFichero(tListaEdificios &lista)
{
	tEdificio aux;
	int aux_estado;
	ifstream ed;
	cstring limpiar;

	ed.open("edificios.txt");
	if (ed.is_open())
	{
		lista.contador=0;

		ed >> aux.codigo;

		while(aux.codigo!=-1)
		{
			//getline(cin,ed, limpiar);
			ed.get(aux.nombre, 200, '\n');
			//getline(ed, limpiar);
			ed >> aux.precio;
			ed >> aux.dinero;
			ed >> aux.prestigio;
			ed >> aux_estado;
			aux.estadoEdificio = deValorAEstadoEdificio(aux_estado);
			lista.edificios[lista.contador]= aux;
			lista.contador++;
			ed >> aux.codigo;
		}
		cout << "Se insertaron " << lista.contador << " edificios con exito!" << endl;
	}
	else
	{
		cout << "Archivo edificios.txt no encontrado!" << endl;
	}
}*/

void mostrarEdificio(tEdificio &edificio)
{
	cout << "\t Identificador: " << edificio.codigo <<endl;
	cout << "\t Nombre: " <<  edificio.nombre  <<endl;;
	cout << "\t Precio de compra: "<<   edificio.precio  <<endl;;
	cout << "\t Dinero por turno : " <<  edificio.dinero  <<endl;;
	cout << "\t Prestigio por turno : "  <<  edificio.prestigio   <<endl;;
	cout <<  "\t Estado: "<<  deEstadoEdificioAcstring(edificio.estadoEdificio)  <<endl; ;


}
tEstadoEdificio deValorAEstadoEdificio(int valor)
{
	tEstadoEdificio nuevo_estado;
	switch(valor)
	{
	case 0:
		nuevo_estado=disponible;
		break;
	case 1:
		nuevo_estado=comprado;
		break;
	case 2:
		nuevo_estado=borrado;
		break;
	}
	return nuevo_estado;

}
