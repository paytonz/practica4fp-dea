#include <iostream>
#include <string>
#include <stdio.h>
#include <string.h>

#include "listaEdificios.h"
using namespace std;

bool bajaEdificio(tListaEdificios &listaEdificios,int codigo)
{
    //pide al admin un codigo y lo procesa,si ese codigo existe entre los codigos del fichero edificios
    //lo borra (se marca como borrado) el edificio se borrara si no se ha comprado por nadie
    int pos = buscarEdificio(listaEdificios,codigo);

    //busca la posicion del edificio y la marca como borrada
    if ( pos != -1 ){
        if (listaEdificios.edificios[pos].estadoEdificio == comprado) {
            listaEdificios.edificios[pos].estadoEdificio = borrado;
        }
        else{
            for (int i = pos; i < listaEdificios.contador - 2; i++) {
                listaEdificios.edificios[pos] = listaEdificios.edificios[pos+1];
            }
        }
        return true;
    }
    /*comprobamos que el edificio no esta comprado
    **si esta comprado -> marcamos como borrado
    **si no lo esta -> procedemos a borrar.
    */
    return false;
}

int buscarEdificio(tListaEdificios &edificios,int codigo)
{
    int total= edificios.contador;
    int mitad = total /2 ;


    if (edificios.edificios[mitad].codigo==codigo) return mitad;
    else if(edificios.edificios[mitad].codigo<codigo) return buscarEdificio(edificios,codigo,mitad+1,total);
    else return buscarEdificio(edificios,codigo,0,mitad);

}

// Implementacion recursiva
int buscarEdificio(tListaEdificios &edificios,int codigo,int a , int b)
{
    int mitad = (b+a) /2 ;

    if (b>a)
    {
        if (edificios.edificios[mitad].codigo==codigo) return mitad;
        else if(edificios.edificios[mitad].codigo<codigo) return buscarEdificio(edificios,codigo,mitad+1,b);
        else return buscarEdificio(edificios,codigo,a,mitad);
    }
    return -1;
}

bool listaEdificiosLlena (tListaEdificios edificios){
	bool llena = false;

	if (edificios.edificios[LISTA_EDIFICIOS_MAX].codigo == -1) 
		llena = true;

	return llena;
}
	
void listadoEdificios(tListaEdificios &edificios)
{
    for(int i=0; i < edificios.contador; i++  )
    {
        cout << "================================================" << endl;
        mostrarEdificio(edificios.edificios[i]);
    }
}

/*void mostrarEdificio(tEdificio &edificio)
{
	cout << "\t Identificador: " << edificio.codigo <<endl;
	cout << "\t Nombre: " <<  edificio.nombre  <<endl;;
	cout << "\t Precio de compra: "<<   edificio.precio  <<endl;;
	cout << "\t Dinero por turno : " <<  edificio.dinero  <<endl;;
	cout << "\t Prestigio por turno : "  <<  edificio.prestigio   <<endl;;
	cout <<  "\t Estado: "<<  deEstadoEdificioAcstring(edificio.estadoEdificio)  <<endl; ;

}*/
char* deEstadoEdificioAcstring(tEstadoEdificio e){
	char * resultado= new char[50];
	if (e==0) strcpy(resultado,"DISPONIBLE");
	if (e==1) strcpy (resultado,"COMPRADO");
	if (e==2) strcpy (resultado,"BORRADO");

	return resultado;
}

