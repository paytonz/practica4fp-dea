#ifndef EDIFICIO_H
#define EDIFICIO_H

#include "utils.h"

#include <iostream>
#include <string.h>

using namespace std;
#define LISTA_EDIFICIOS_MAX 50

typedef enum {disponible,comprado, borrado}  tEstadoEdificio;

/*----M�dulo de Edificio------*/
const int MAX_CHARS = 30;
typedef char tCadena[MAX_CHARS];

typedef struct{
    int codigo;
    tCadena nombre;
    int precio;
    double dinero;
    double prestigio;
    tEstadoEdificio estadoEdificio;
} tEdificio;

/******************************************** CABECERAS M�DULOS EDIFICIO ********************************************/

/*No recibe ning�n valor de entrada. Pide al usuario los datos de un edificio y devuelve como salida una estructura tEdificio debidamente inicializada.*/
tEdificio nuevoEdificio();

/*Dado un tEdificio muestra todos sus datos en la consola, debidamente formateados. */
void mostrarEdificio(tEdificio&);

tEstadoEdificio deValorAEstadoEdificio(int valor);
#endif