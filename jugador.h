#ifndef JUGADOR_H
#define JUGADOR_H

#include <string>

//#include "utils.h"
#include "listaEdificios.h"
#include "listaJugadores.h"

using namespace std;

const int MAX_COMPRADOS = 20;
typedef int tComprados[MAX_COMPRADOS];

typedef struct {
	tComprados comprados;
	int contador;
} tListaComprados;


typedef struct {
	tCadena usuario;
	tCadena contrasenya;
	tCadena universidad;
	int dinero;
	int prestigio;
	tListaComprados edificiosComprados[MAX_COMPRADOS];
} tJugador;

/*typedef struct{
    tCadena nombre;
    tCadena contrasenya;
    tCadena universidad;
    int dinero;
    int prestigio;
    int edificiosComprados[LISTA_EDIFICIOS_MAX]; //la lista debe ser de tama�o variable
} tJugador;
*/
tJugador jugadores;

/*Lee los datos de un jugador, inicializa su lista de edificios comprados como vacia, el dinero a 3000 creditos y los puntos de prestigio a 0
Devuelve la estructura tJugador debidamente inicializada*/
void nuevoJugador(tJugador &jugador);

//Dado un tJugador y la lista de edificios, muestra en la consola los datos del jugador y los datos basicos del jugador y los datos basicos de todos sus edificios
//(nombre, dinero y prestigio por turno
void mostrarJugador(const tJugador jugador, tListaEdificios &edificios, bool mostrarInfo);

//Recibe como entrada un tJugador y devuelve un booleano indicando si su lista de edificios comprados esta llena
bool listaCompradosLlena(const tJugador &jug, bool &llena);

//Recibe como entrada un tJugador, un codigo de edificio y la lista completa de edificios. Si se puede hacer la compra (el edicio existe,
//esta disponible y hay dinero suficiente) realiza la compra y a�ade el codigo de edificio a la lista de edificios del jugador
void comprarEdificio(const tJugador &jug, int codigo, tListaEdificios &edificios);

#endif
