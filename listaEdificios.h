#ifndef LISTAEDIFICIOS_H
#define LISTAEDIFICIOS_H
#include <string>
#include <iostream>
#include "edificio.h"

using namespace std;

typedef struct{
    tEdificio edificios[LISTA_EDIFICIOS_MAX];
    int contador;
} tListaEdificios;

//funcion que recibe como parametro una lista de edificios y devuelve un booleano indicando si la lista esta llena o no
bool listaEdificiosLlena(const tListaEdificios &edificios);

/*Recibe una lista de edificios y un c�digo. Indica si se ha encontrado o no un edificio con ese c�digo y, en caso afirmativo, la posici�n en
la que se encuentra. Debe estar implementado como una b�squeda binaria. */
int buscarEdificio(tListaEdificios &edificios ,int codigo);

/* Funcion auxiliar */
int buscarEdificio(tListaEdificios &edificios,int codigo,int a,int b);

/* Recibe un tEdificio y una lista de edificios. Inserta el edificio en la lista en la posici�n que le corresponda por c�digo y devuelve la
lista actualizada.*/
tListaEdificios insertarEdificio(tEdificio edif, tListaEdificios edificios);

/*Recibe la lista edificios y un c�digo de edificio. Si el edificio existe y est� disponible, lo marca como borrado en la lista.*/
bool bajaEdificio(tListaEdificios &edificios,int codigo);
/*
/* Lista todos los edificios de nuestra lista debidamente formateados 
void listadoEdificios(tListaEdificios &edificios);
*/
char* deEstadoEdificioAcstring(tEstadoEdificio e);
void listadoEdificios(tListaEdificios &edificios);
#endif


