#include <iostream>
#include <fstream>
#include <string>
using namespace std;

#include "utils.h"
#include "listaEdificios.h"
#include "listaJugadores.h"

void menuJuego(int &op);
void menuAdministracion(int &op);

void leerArchivoJug(tListaJugadores &listaJug)
{
};
void guardarArchivoJug(const tListaJugadores &listaJug);
void guardarEnFichero(tListaEdificios &edificios);
void cargaFichero(tListaEdificios &edificios);
void eleccionMenu();

int main()
{

	tListaEdificios edificios;
	tListaJugadores listaJug;

	leerArchivoJug(listaJug);
	cargaFichero(edificios);

	eleccionMenu();

	//guardarArchivoJug(listaJug);
	//guardarEnFichero(edificios);

	cin.sync();
	cin.get();
	return 0;
}

void eleccionMenu(){

	bool encontrado, correcto;
	int posicion;
	int op, codigo;
	cstring nombre, contrasenya;
	tListaJugadores listaJug;
	tListaEdificios listaEdificios;
	tListaEdificios edificios;

	do{
		cout << "Usuario: ";
		GETLINE(cin, nombre);

		buscarJugador(listaJug, nombre, encontrado, posicion);

		if (encontrado){
			correcto = true;
			menuJuego(op);
			do{
				switch(op){
				case 1: //Ver mis edificios; 
					break;
				case 2: //Ver los edificios disponibles; 
					break;
				case 3: //Comprar un edificio; 
					break;
				case 4:// Ver la clasificacion (ordenada por dinero) ; 
					break;
				case 5: //Ver la clasificacion (ordenada por prestigio); 
					break;
				case 0: //Cerrar la sesion ; 
					break;


								} //switch
			}while (op != 0);
		}
		else{
			if (nombre == "admin")
			{
				cout << "Password: ";
				cin.getline(contrasenya, CSTRING_LENGHT);
				if (contrasenya == "12345")
				{
					correcto = true;
					menuAdministracion(op);
					do {
						switch(op){
						case 1:  listadoEdificios(edificios);//Ver el listado de edificios ; 
							break;
						case 2: nuevoEdificio();//Nuevo edificio; 
							break;
						case 3:bajaEdificio(listaEdificios, codigo);//Borrar un edificio ; 
							break;
						case 4: //Ver el listado de jugadores; 
							break;
						case 5: //Nuevo jugador; 
							break;
						case 6: // Borrar un jugador"; 
							break;
						case 7: // Ejecutar un turno; 
							break;
						case 8://Ver la clasificacion ;
							break;
						case 0://Cerrar la sesion ; 
							break;
						}//switch

					}while (op != 0);

				} else correcto = false; //if
		}//else
		} //if grande
	}while (correcto == false);//do while grande
}//cierre de funcion

void menuAdministracion(int &op){

	do{
		cout << endl;
		cout << "--- MENU ADMINISTRACION ---" << endl;
		cout << "1. Ver el listado de edificios" << endl;
		cout << "2. Nuevo edificio" << endl;
		cout << "3. Borrar un edificio" << endl;
		cout << "4. Ver el listado de jugadores" << endl;
		cout << "5. Nuevo jugador" << endl;
		cout << "6. Borrar un jugador" << endl;
		cout << "7. Ejecutar un turno" << endl;
		cout << "8. Ver la clasificacion" << endl;
		cout << "0. Cerrar la sesion" << endl;
		cout << endl;

		cin.sync();
		cout << "Elija una opcion: ";
		cin >> op;
		cout << endl;

		cin.sync();

	} while (op < 0 || op > 8);
}

void menuJuego(int &op){

	do
	{
		cout << endl;
		cout << "--- MENU DE JUEGO ---" << endl;
		cout << "1. Ver mis edificios" << endl;
		cout << "2. Ver los edificios disponibles" << endl;
		cout << "3. Comprar un edificio" << endl;
		cout << "4. Ver la clasificacion (ordenada por dinero)" << endl;
		cout << "5. Ver la clasificacion (ordenada por prestigio)" << endl;
		cout << "0. Cerrar la sesion" << endl;
		cout << endl;

		cin.sync();
		cout << "Elija una opcion: ";
		cin >> op;
		cout << endl;

		cin.sync();

	} while (op < 0 || op > 5);
}

void leerArchivoJug(tListaJugadores &listaJug, tListaEdificios& edificios){
	ifstream archivo;
	archivo.open("jugadores.txt");
	cstring nombre;

	int cont = 0;
	int cont2 = -1;
	int codigo;

	listaJug.contador = 0;

	if (!archivo.is_open()) cout << "El archivo no existe. El programa empezara con una lista vacia" << endl;
	else
	{
		cout << "Archivo de jugadores cargado con exito" << endl;

		while (cont < LISTA_JUGADORES_MAX)
		{
			GETLINE(archivo, nombre);
			while (nombre != "x" || nombre != "X")
			{
				//				archivo.getline(listaJug.jugadores[cont].nombre , nombre);
				archivo >> nombre;
			}
			archivo >> listaJug.jugadores[cont].contrasenya;
			//			archivo.getline(archivo, listaJug.jugadores[cont].universidad);
			archivo >> listaJug.jugadores[cont].dinero;
			archivo >> listaJug.jugadores[cont].prestigio;
			archivo >> codigo;

			while (codigo != -1 && ++cont2 < LISTA_EDIFICIOS_MAX)
			{
				edificios.edificios[listaJug.jugadores[cont].edificios[cont2]].codigo = codigo;
			}

			cont++;
			listaJug.contador++;
		}
	}

	archivo.close();
}

void cargaFichero(tListaEdificios &edificios){

	tEdificio aux;
	int aux_estado;
	ifstream ed;
	cstring limpiar;

	ed.open("edificios.txt");
	if (ed.is_open()){

		edificios.contador = 0;

		ed >> aux.codigo;

		while (aux.codigo != -1){
			GETLINE(ed, limpiar);
			GETLINE(ed, aux.nombre);
			GETLINE(ed, limpiar);
			ed >> aux.precio;
			ed >> aux.dinero;
			ed >> aux.prestigio;
			ed >> aux_estado;

			aux.estadoEdificio = (tEstadoEdificio) aux_estado;
			edificios.edificios[edificios.contador] = aux;
			edificios.contador++;
			ed >> aux.codigo;
		}
		cout << "Se insertaron " << edificios.contador << " edificios con exito!" << endl;
	}
	else
	{
		cout << "Archivo edificios.txt no encontrado!" << endl;
	}

}

void guardarArchivoJug(const tListaJugadores &listaJug, tListaEdificios& edificios)
{

	ofstream archivo;
	archivo.open("jugadores.txt");

	if (archivo.is_open())
	{
		for (int i = 0; i < listaJug.contador; i++)
		{
			archivo << listaJug.jugadores[i].nombre << endl;
			archivo << listaJug.jugadores[i].contrasenya << endl;
			archivo << listaJug.jugadores[i].universidad << endl;
			archivo << listaJug.jugadores[i].dinero << endl;
			archivo << listaJug.jugadores[i].prestigio << endl;

			for (unsigned int j = 0; j < LISTA_EDIFICIOS_MAX; ++j)
				archivo << edificios.edificios[listaJug.jugadores[i].edificios[j]].codigo << endl;

			archivo << -1 << endl;
		}

		archivo << "X" << endl;

		cout << "Archivo guardado" << endl;
		cout << "Jugadores guardados" << endl;
	}
	else
	{
		cout << "Error de apertura" << endl;
	}

	archivo << -1 << endl;
	archivo << "X" << endl;
	archivo.close();
}
/*
void guardarEnFichero(tListaEdificios &edificios){

tEdificio nuevo_edificio;
//int nuevo_edificio_estado;
ofstream ed;
//cstring limpiar;

ed.open("edificios.txt");
if (!ed.is_open()){
cout << "Archivo edificios.txt no encontrado!" << endl;
}
else{
ed << nuevo_edificio.codigo;
ed << nuevo_edificio.nombre;
ed << nuevo_edificio.precio;
ed << nuevo_edificio.dinero;
ed << nuevo_edificio.prestigio;
ed << nuevo_edificio.estadoEdificio;
}
*/
/*nuevo_edificio.estadoEdificio = deValorAEstadoEdificio(nuevo_edificio_estado);
lista.edificios[lista.contador]= nuevo_edificio;
lista.contador++;
ed << nuevo_edificio.codigo;*/
//}
//cout << "Se guardaron " << lista.contador << " edificios con exito!" << endl;
//}
//else
//{

//}
